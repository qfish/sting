//
//     ____    _                        __     _      _____
//    / ___\  /_\     /\/\    /\ /\    /__\   /_\     \_   \
//    \ \    //_\\   /    \  / / \ \  / \//  //_\\     / /\/
//  /\_\ \  /  _  \ / /\/\ \ \ \_/ / / _  \ /  _  \ /\/ /_
//  \____/  \_/ \_/ \/    \/  \___/  \/ \_/ \_/ \_/ \____/
//
//	Copyright Samurai development team and other contributors
//
//	http://www.samurai-framework.com
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy
//	of this software and associated documentation files (the "Software"), to deal
//	in the Software without restriction, including without limitation the rights
//	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//	copies of the Software, and to permit persons to whom the Software is
//	furnished to do so, subject to the following conditions:
//
//	The above copyright notice and this permission notice shall be included in
//	all copies or substantial portions of the Software.
//
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//	THE SOFTWARE.
//

#import "Samurai_EventHandler.h"
#import "Samurai_Property.h"
#import <objc/objc.h>

// ----------------------------------
// Source code
// ----------------------------------

#pragma mark -

typedef void (^ __handlerBlockType )( id object );

#pragma mark -

@implementation NSObject(EventHandler)

- (SamuraiEventHandler *)eventHandlerOrCreate
{
	SamuraiEventHandler * handler = [self getAssociatedObjectForKey:"eventHandler"];
	
	if ( nil == handler )
	{
		handler = [[SamuraiEventHandler alloc] init];
		
		[self retainAssociatedObject:handler forKey:"eventHandler"];
	}
	
	return handler;
}

- (SamuraiEventHandler *)eventHandler
{
	return [self getAssociatedObjectForKey:"eventHandler"];
}

- (void)addHandlerBlock:(id)block forEvent:(NSString *)name
{
	SamuraiEventHandler * handler = [self eventHandlerOrCreate];
	if ( handler )
	{
		[handler addHandler:block forEvent:name];
	}
}

- (void)removeHandlerBlockForEvent:(NSString *)name
{
	SamuraiEventHandler * handler = [self eventHandler];
	if ( handler )
	{
		[handler removeHandlerForEvent:name];
	}
}

@end

#pragma mark -

@implementation SamuraiEventHandler
{
	NSMutableDictionary * _blocks;
}

- (id)init
{
	self = [super init];
	if ( self )
	{
		_blocks = [[NSMutableDictionary alloc] init];
	}
	return self;
}

- (void)dealloc
{
	[_blocks removeAllObjects];
	_blocks = nil;
}

- (BOOL)sendEvent:(NSString *)event
{
	return [self sendEvent:event withObject:nil];
}

- (BOOL)sendEvent:(NSString *)event withObject:(id)object
{
	if ( nil == event )
		return NO;

	__handlerBlockType block = (__handlerBlockType)[_blocks objectForKey:event];
	if ( nil == block )
		return NO;

	block( object );
	return YES;
}

- (void)addHandler:(id)handler forEvent:(NSString *)event
{
	if ( nil == event )
		return;

	if ( nil == handler )
	{
		[_blocks removeObjectForKey:event];
	}
	else
	{
		[_blocks setObject:handler forKey:event];
	}
}

- (void)removeHandlerForEvent:(NSString *)event
{
	if ( nil == event )
		return;

	[_blocks removeObjectForKey:event];
}

- (void)removeAllHandlers
{
	[_blocks removeAllObjects];
}

@end
