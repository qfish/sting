//
//     ____    _                        __     _      _____
//    / ___\  /_\     /\/\    /\ /\    /__\   /_\     \_   \
//    \ \    //_\\   /    \  / / \ \  / \//  //_\\     / /\/
//  /\_\ \  /  _  \ / /\/\ \ \ \_/ / / _  \ /  _  \ /\/ /_
//  \____/  \_/ \_/ \/    \/  \___/  \/ \_/ \_/ \_/ \____/
//
//	Copyright Samurai development team and other contributors
//
//	http://www.samurai-framework.com
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy
//	of this software and associated documentation files (the "Software"), to deal
//	in the Software without restriction, including without limitation the rights
//	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//	copies of the Software, and to permit persons to whom the Software is
//	furnished to do so, subject to the following conditions:
//
//	The above copyright notice and this permission notice shall be included in
//	all copies or substantial portions of the Software.
//
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//	THE SOFTWARE.
//
#import "Samurai_Predefine.h"
#import "Samurai_Property.h"

#pragma mark -

#undef	NSLog
#define	NSLog	SamuraiLog

#pragma mark -

typedef enum
{
	LogLevel_Error = 0,
	LogLevel_Warn,
	LogLevel_Info,
	LogLevel_Perf,
	LogLevel_All
} LogLevel;

#pragma mark -

#if __SAMURAI_LOGGING__

#if __SAMURAI_DEBUG__

	#define INFO( ... )		[[SamuraiLogger sharedInstance] file:@(__FILE__) line:__LINE__ func:@(__PRETTY_FUNCTION__) level:LogLevel_Info format:__VA_ARGS__];
	#define PERF( ... )		[[SamuraiLogger sharedInstance] file:@(__FILE__) line:__LINE__ func:@(__PRETTY_FUNCTION__) level:LogLevel_Perf format:__VA_ARGS__];
	#define WARN( ... )		[[SamuraiLogger sharedInstance] file:@(__FILE__) line:__LINE__ func:@(__PRETTY_FUNCTION__) level:LogLevel_Warn format:__VA_ARGS__];
	#define ERROR( ... )	[[SamuraiLogger sharedInstance] file:@(__FILE__) line:__LINE__ func:@(__PRETTY_FUNCTION__) level:LogLevel_Error format:__VA_ARGS__];
	#define PRINT( ... )	[[SamuraiLogger sharedInstance] file:@(__FILE__) line:__LINE__ func:@(__PRETTY_FUNCTION__) level:LogLevel_All format:__VA_ARGS__];

#else

	#define INFO( ... )		[[SamuraiLogger sharedInstance] file:nil line:0 func:nil level:LogLevel_Info format:__VA_ARGS__];
	#define PERF( ... )		[[SamuraiLogger sharedInstance] file:nil line:0 func:nil level:LogLevel_Perf format:__VA_ARGS__];
	#define WARN( ... )		[[SamuraiLogger sharedInstance] file:nil line:0 func:nil level:LogLevel_Warn format:__VA_ARGS__];
	#define ERROR( ... )	[[SamuraiLogger sharedInstance] file:nil line:0 func:nil level:LogLevel_Error format:__VA_ARGS__];
	#define PRINT( ... )	[[SamuraiLogger sharedInstance] file:nil line:0 func:nil level:LogLevel_All format:__VA_ARGS__];

#endif

#else

	#define INFO( ... )
	#define PERF( ... )
	#define WARN( ... )
	#define ERROR( ... )
	#define PRINT( ... )

#endif

#ifndef	VAR_DUMP
#define VAR_DUMP( __obj )	PRINT( [__obj description] );
#endif

#ifndef	OBJ_DUMP
#define OBJ_DUMP( __obj )	PRINT( [__obj objectToDictionary] );
#endif

#pragma mark -

@interface SamuraiBacklog : NSObject

@prop_assign( LogLevel,		level );
@prop_strong( NSString *,	file );
@prop_assign( NSUInteger,	line );
@prop_strong( NSString *,	func );
@prop_strong( NSDate *,		time );
@prop_strong( NSString *,	text );

@end

#pragma mark -

@interface SamuraiLogger : NSObject

@singleton( SamuraiLogger );

@prop_assign( LogLevel,		filter );
@prop_strong( NSArray *,	logs );
@prop_strong( NSLock *,		lock );
@prop_assign( BOOL,			enabled );

- (void)toggle;
- (void)enable;
- (void)disable;

- (void)indent;
- (void)indent:(NSUInteger)tabs;
- (void)unindent;
- (void)unindent:(NSUInteger)tabs;

- (void)file:(NSString *)file line:(NSUInteger)line func:(NSString *)func level:(LogLevel)level format:(NSString *)format, ...;
- (void)file:(NSString *)file line:(NSUInteger)line func:(NSString *)func level:(LogLevel)level format:(NSString *)format args:(va_list)args;

@end

#pragma mark -

#if __cplusplus
extern "C" {
#endif

void SamuraiLog( NSString * format, ... );

#if __cplusplus
}
#endif
