//
//     ____    _                        __     _      _____
//    / ___\  /_\     /\/\    /\ /\    /__\   /_\     \_   \
//    \ \    //_\\   /    \  / / \ \  / \//  //_\\     / /\/
//  /\_\ \  /  _  \ / /\/\ \ \ \_/ / / _  \ /  _  \ /\/ /_
//  \____/  \_/ \_/ \/    \/  \___/  \/ \_/ \_/ \_/ \____/
//
//	Copyright Samurai development team and other contributors
//
//	http://www.samurai-framework.com
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy
//	of this software and associated documentation files (the "Software"), to deal
//	in the Software without restriction, including without limitation the rights
//	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//	copies of the Software, and to permit persons to whom the Software is
//	furnished to do so, subject to the following conditions:
//
//	The above copyright notice and this permission notice shall be included in
//	all copies or substantial portions of the Software.
//
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//	THE SOFTWARE.
//

#pragma mark -

typedef NSObject *	(^ SamuraiEventObserverBlock )( NSString * name, id object );
typedef NSObject *	(^ SamuraiKeyValueObserverBlock )( id nameOrObject, id propertyOrBlock, ... );

#pragma mark -

@class SamuraiEventHandler;

@interface NSObject(EventHandler)

- (SamuraiEventHandler *)eventHandlerOrCreate;
- (SamuraiEventHandler *)eventHandler;

- (void)addHandlerBlock:(id)block forEvent:(NSString *)name;
- (void)removeHandlerBlockForEvent:(NSString *)name;

@end

#pragma mark -

@interface SamuraiEventHandler : NSObject

- (BOOL)sendEvent:(NSString *)event;
- (BOOL)sendEvent:(NSString *)event withObject:(id)object;

- (void)addHandler:(id)handler forEvent:(NSString *)event;
- (void)removeHandlerForEvent:(NSString *)event;
- (void)removeAllHandlers;

@end
