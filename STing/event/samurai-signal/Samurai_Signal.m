//
//     ____    _                        __     _      _____
//    / ___\  /_\     /\/\    /\ /\    /__\   /_\     \_   \
//    \ \    //_\\   /    \  / / \ \  / \//  //_\\     / /\/
//  /\_\ \  /  _  \ / /\/\ \ \ \_/ / / _  \ /  _  \ /\/ /_
//  \____/  \_/ \_/ \/    \/  \___/  \/ \_/ \_/ \_/ \____/
//
//	Copyright Samurai development team and other contributors
//
//	http://www.samurai-framework.com
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy
//	of this software and associated documentation files (the "Software"), to deal
//	in the Software without restriction, including without limitation the rights
//	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//	copies of the Software, and to permit persons to whom the Software is
//	furnished to do so, subject to the following conditions:
//
//	The above copyright notice and this permission notice shall be included in
//	all copies or substantial portions of the Software.
//
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//	THE SOFTWARE.
//

#import "Samurai_Signal.h"
#import "Samurai_SignalBus.h"
#import "Samurai_Object.h"
#import "Samurai_Log.h"
#import <objc/objc.h>

// ----------------------------------
// Source code
// ----------------------------------

#pragma mark -

@implementation NSObject(SignalResponder)

@def_prop_dynamic( SamuraiEventBlock, onSignal );

#pragma mark -

hookAfter( load, SignalResponder )
{
}

hookBefore( unload, SignalResponder )
{
	NSMutableArray * responders = [self getAssociatedObjectForKey:"signalResponders"];
	if ( responders )
	{
		[responders removeAllObjects];
		
		[self removeAssociatedObjectForKey:"signalResponders"];
	}
}

#pragma mark -

- (SamuraiEventObserverBlock)onSignal
{
	@weakify( self );
	
	SamuraiEventObserverBlock block = ^ NSObject * ( NSString * name, id signalBlock )
	{
		@strongify(self);
		
		name = [name stringByReplacingOccurrencesOfString:@"signal." withString:@"handleSignal____"];
		name = [name stringByReplacingOccurrencesOfString:@"signal____" withString:@"handleSignal____"];
		name = [name stringByReplacingOccurrencesOfString:@"-" withString:@"____"];
		name = [name stringByReplacingOccurrencesOfString:@"." withString:@"____"];
		name = [name stringByReplacingOccurrencesOfString:@"/" withString:@"____"];
		name = [name stringByAppendingString:@":"];

		if ( signalBlock )
		{
			[self addHandlerBlock:signalBlock forEvent:name];
		}
		else
		{
			[self removeHandlerBlockForEvent:name];
		}

		return self;
	};
	
	return [block copy];
}

#pragma mark -

- (id)signalResponders
{
	const char * storeKey = "signalResponders";
	
	NSMutableArray * responders = [self getAssociatedObjectForKey:storeKey];
	if ( nil == responders )
	{
		responders = [NSMutableArray nonRetainingArray];
		[self retainAssociatedObject:responders forKey:storeKey];
	}
	return responders;
}

- (id)signalAlias
{
	return nil;
}

- (BOOL)hasSignalResponder:(id)obj
{
	NSObject *		object = [self signalResponders];
	
	if ( nil == object )
	{
		return NO;
	}
	else
	{
		if ( [object isKindOfClass:[NSArray class]] )
		{
			NSArray * responders = (NSArray *)object;
			
			for ( id responder in responders )
			{
				if ( responder == obj )
				{
					return YES;
				}
			}
		}
		else
		{
			if ( obj == object )
			{
				return YES;
			}
		}
	}

	return NO;
}

- (void)addSignalResponder:(id)obj
{
	NSObject * object = [self signalResponders];
	if ( object && [object isKindOfClass:[NSMutableArray class]] )
	{
		NSMutableArray * responders = (NSMutableArray *)object;
		if ( NO == [responders containsObject:obj] )
		{
			[responders addObject:obj];
		}
	}
}

- (void)removeSignalResponder:(id)obj
{
	NSObject * object = [self signalResponders];
	if ( object && [object isKindOfClass:[NSMutableArray class]] )
	{
		NSMutableArray * responders = (NSMutableArray *)object;
		if ( [responders containsObject:obj] )
		{
			[responders removeObject:obj];
		}
	}
}

- (void)removeAllSignalResponders
{
	NSObject * object = [self signalResponders];
	if ( object && [object isKindOfClass:[NSMutableArray class]] )
	{
		NSMutableArray * responders = (NSMutableArray *)object;
		[responders removeAllObjects];
	}
}

- (NSString *)signalNamespace
{
	return NSStringFromClass([self class]);
}

- (NSString *)signalTag
{
	return nil;
}

- (NSString *)signalDescription
{
	return [NSString stringWithFormat:@"%@", [[self class] description]];
}

- (void)handleSignal:(SamuraiSignal *)that
{
	UNUSED( that );
}

@end

#pragma mark -

@implementation NSObject(SignalSender)

- (SamuraiSignal *)sendSignal:(NSString *)name
{
	return [self sendSignal:name from:self withObject:nil];
}

- (SamuraiSignal *)sendSignal:(NSString *)name withObject:(NSObject *)object
{
	return [self sendSignal:name from:self withObject:object];
}

- (SamuraiSignal *)sendSignal:(NSString *)name from:(id)source
{
	return [self sendSignal:name from:source withObject:nil];
}

- (SamuraiSignal *)sendSignal:(NSString *)name from:(id)source withObject:(NSObject *)object
{
	SamuraiSignal * signal = [SamuraiSignal signal];
	
	signal.source = source ? source : self;
	signal.target = self;
	signal.name = name;
	signal.object = object;
	
	[signal send];
	
	return signal;
}

@end

#pragma mark -

@implementation SamuraiSignal

@def_prop_unsafe( id,						foreign );
@def_prop_unsafe( id,						source );
@def_prop_unsafe( id,						target );

@def_prop_coping( BlockTypeVoid,			stateChanged );
@def_prop_assign( SignalState,				state );
@def_prop_dynamic( BOOL,					sending );
@def_prop_dynamic( BOOL,					arrived );
@def_prop_dynamic( BOOL,					dead );

@def_prop_assign( BOOL,						hit );
@def_prop_assign( NSUInteger,				hitCount );
@def_prop_dynamic( NSString *,				prettyName );

@def_prop_strong( NSString *,				name );
@def_prop_strong( NSString *,				prefix );
@def_prop_strong( id,						object );
@def_prop_strong( NSMutableDictionary *,	input );
@def_prop_strong( NSMutableDictionary *,	output );

@def_prop_assign( NSTimeInterval,			initTimeStamp );
@def_prop_assign( NSTimeInterval,			sendTimeStamp );
@def_prop_assign( NSTimeInterval,			arriveTimeStamp );

@def_prop_dynamic( NSTimeInterval,			timeElapsed );
@def_prop_dynamic( NSTimeInterval,			timeCostPending );
@def_prop_dynamic( NSTimeInterval,			timeCostExecution );

@def_prop_assign( NSInteger,				jumpCount );
@def_prop_strong( NSArray *,				jumpPath );

#pragma mark -

+ (SamuraiSignal *)signal
{
	return [[SamuraiSignal alloc] init];
}

+ (SamuraiSignal *)signal:(NSString *)name
{
	SamuraiSignal * signal = [[SamuraiSignal alloc] init];
	signal.name = name;
	return signal;
}

- (SamuraiSignal *)spawn
{
	SamuraiSignal * signal = [[SamuraiSignal alloc] init];
	
	signal.foreign = self.foreign;
	signal.source = self.source;
	signal.target = self.target;
	
	signal.state = self.state;
	
	signal.name = [self.name copy];
	signal.prefix = [self.prefix copy];
	signal.object = self.object;

	signal.initTimeStamp = self.initTimeStamp;
	signal.sendTimeStamp = self.sendTimeStamp;
	signal.arriveTimeStamp = self.arriveTimeStamp;
	
	signal.jumpCount = self.jumpCount;
	signal.jumpPath = [self.jumpPath mutableCopy];
	
	return signal;
}

- (id)init
{
	static NSUInteger __seed = 0;

	self = [super init];
	if ( self )
	{
		self.name = [NSString stringWithFormat:@"signal-%lu", (unsigned long)__seed++];

		_state = SignalState_Init;

		_initTimeStamp = [NSDate timeIntervalSinceReferenceDate];
		_sendTimeStamp = _initTimeStamp;
		_arriveTimeStamp = _initTimeStamp;
	}
	return self;
}

- (void)dealloc
{
	self.jumpPath = nil;
	self.stateChanged = nil;

	self.name = nil;
	self.prefix = nil;
	self.object = nil;
	
	self.input = nil;
	self.output = nil;
}

- (NSString *)prettyName
{
	return [self.name stringByReplacingOccurrencesOfString:@"signal." withString:@""];
}

- (NSString *)description
{
#if __SAMURAI_DEBUG__
	return [NSString stringWithFormat:@"[%@] > %@", self.prettyName, [self.jumpPath join:@" > "]];
#else
	return self.name;
#endif
}

- (BOOL)is:(NSString *)name
{
	return [self.name isEqualToString:name];
}

- (BOOL)isSentFrom:(id)source
{
	return (self.source == source) ? YES : NO;
}

- (SignalState)state
{
	return _state;
}

- (void)setState:(SignalState)newState
{
	[self changeState:newState];
}

- (BOOL)sending
{
	return SignalState_Sending == _state ? YES : NO;
}

- (void)setSending:(BOOL)flag
{
	if ( flag )
	{
		[self changeState:SignalState_Sending];
	}
}

- (BOOL)arrived
{
	return SignalState_Arrived == _state ? YES : NO;
}

- (void)setArrived:(BOOL)flag
{
	if ( flag )
	{
		[self changeState:SignalState_Arrived];
	}
}

- (BOOL)dead
{
	return SignalState_Dead == _state ? YES : NO;
}

- (void)setDead:(BOOL)flag
{
	if ( flag )
	{
		[self changeState:SignalState_Dead];
	}
}

- (BOOL)changeState:(SignalState)newState
{
	if ( newState == _state )
		return NO;

	triggerBefore( self, stateChanged );

	INFO( @"signal %@, state %d -> %d", self.prettyName, _state, newState );

	_state = newState;

	if ( SignalState_Sending == _state )
	{
		_sendTimeStamp = [NSDate timeIntervalSinceReferenceDate];
	}
	else if ( SignalState_Arrived == _state )
	{
		_arriveTimeStamp = [NSDate timeIntervalSinceReferenceDate];
	}
	else if ( SignalState_Dead == _state )
	{
		_arriveTimeStamp = [NSDate timeIntervalSinceReferenceDate];
	}
	
	if ( self.stateChanged )
	{
		self.stateChanged();
	}

	triggerAfter( self, stateChanged );

	return YES;
}

- (BOOL)send
{
	@autoreleasepool
	{
		return [[SamuraiSignalBus sharedInstance] send:self];
	};
}

- (BOOL)forward
{
	@autoreleasepool
	{
		return [[SamuraiSignalBus sharedInstance] forward:self];
	};
}

- (BOOL)forward:(id)target
{
	@autoreleasepool
	{
		return [[SamuraiSignalBus sharedInstance] forward:self to:target];
	};
}

- (NSTimeInterval)timeElapsed
{
	return _arriveTimeStamp - _initTimeStamp;
}

- (NSTimeInterval)timeCostPending
{
	return _sendTimeStamp - _initTimeStamp;
}

- (NSTimeInterval)timeCostExecution
{
	return _arriveTimeStamp - _sendTimeStamp;
}

- (void)log:(id)target
{
	if ( self.arrived || self.dead )
		return;

#if __SAMURAI_DEBUG__
	if ( target )
	{
		if ( nil == self.jumpPath )
		{
			self.jumpPath = [[NSMutableArray alloc] init];
		}

		[self.jumpPath addObject:[target signalDescription]];
	}
#endif

	_jumpCount += 1;
}

#pragma mark -

- (NSMutableDictionary *)inputOrOutput
{
	if ( SignalState_Init == _state )
	{
		if ( nil == self.input )
		{
			self.input = [NSMutableDictionary dictionary];
		}
		
		return self.input;
	}
	else
	{
		if ( nil == self.output )
		{
			self.output = [NSMutableDictionary dictionary];
		}
		
		return self.output;
	}
}

- (id)objectForKey:(id)key
{
	NSMutableDictionary * objects = [self inputOrOutput];
	return [objects objectForKey:key];
}

- (BOOL)hasObjectForKey:(id)key
{
	NSMutableDictionary * objects = [self inputOrOutput];
	return [objects objectForKey:key] ? YES : NO;
}

- (void)setObject:(id)value forKey:(id)key
{
	NSMutableDictionary * objects = [self inputOrOutput];
	[objects setObject:value forKey:key];
}

- (void)removeObjectForKey:(id)key
{
	NSMutableDictionary * objects = [self inputOrOutput];
	[objects removeObjectForKey:key];
}

- (void)removeAllObjects
{
	NSMutableDictionary * objects = [self inputOrOutput];
	[objects removeAllObjects];
}

- (id)objectForKeyedSubscript:(id)key;
{
	return [self objectForKey:key];
}

- (void)setObject:(id)obj forKeyedSubscript:(id)key
{
	[self setObject:obj forKey:key];
}

@end
