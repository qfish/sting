//
//     ____    _                        __     _      _____
//    / ___\  /_\     /\/\    /\ /\    /__\   /_\     \_   \
//    \ \    //_\\   /    \  / / \ \  / \//  //_\\     / /\/
//  /\_\ \  /  _  \ / /\/\ \ \ \_/ / / _  \ /  _  \ /\/ /_
//  \____/  \_/ \_/ \/    \/  \___/  \/ \_/ \_/ \_/ \____/
//
//	Copyright Samurai development team and other contributors
//
//	http://www.samurai-framework.com
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy
//	of this software and associated documentation files (the "Software"), to deal
//	in the Software without restriction, including without limitation the rights
//	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//	copies of the Software, and to permit persons to whom the Software is
//	furnished to do so, subject to the following conditions:
//
//	The above copyright notice and this permission notice shall be included in
//	all copies or substantial portions of the Software.
//
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//	THE SOFTWARE.
//

#import "Samurai_Log.h"

// ----------------------------------
// Source code
// ----------------------------------

#pragma mark -

#undef	MAX_BACKLOG
#define MAX_BACKLOG		(64)

static const char * __prefix[] =
{
	"[ERROR]",
	"[WARN]",
	"[INFO]",
	"[PERF]",
	"",
};

#pragma mark -

@implementation SamuraiBacklog

@def_prop_assign( LogLevel,		level );
@def_prop_strong( NSString *,	file );
@def_prop_assign( NSUInteger,	line );
@def_prop_strong( NSString *,	func );
@def_prop_strong( NSDate *,		time );
@def_prop_strong( NSString *,	text );

- (id)init
{
	self = [super init];
	if ( self )
	{
		self.level = LogLevel_Info;
		self.time = [NSDate date];
	}
	return self;
}

- (void)dealloc
{
	self.file = nil;
	self.func = nil;
	self.time = nil;
	self.text = nil;
}

@end

#pragma mark -

@implementation SamuraiLogger
{
	BOOL				_enabled;
	LogLevel			_filter;
	NSLock *			_lock;
	NSMutableArray *	_logs;
	NSUInteger			_indent;
}

@def_singleton( SamuraiLogger );

@def_prop_assign( LogLevel,		filter );
@def_prop_strong( NSArray *,	logs );
@def_prop_strong( NSLock *,		lock );
@def_prop_assign( BOOL,			enabled );

+ (void)classAutoLoad
{
	[SamuraiLogger sharedInstance];
}

- (id)init
{
	self = [super init];
	if ( self )
	{
		self.enabled = YES;
		self.logs = [NSMutableArray new];
		self.lock = [NSLock new];

	#if __SAMURAI_DEBUG__
		self.filter = LogLevel_All;
	#else
		self.filter = LogLevel_Error;
	#endif
	}
	return self;
}

- (void)dealloc
{
	self.logs = nil;
	self.lock = nil;
}

- (void)toggle
{
	_enabled = _enabled ? NO : YES;
}

- (void)enable
{
	_enabled = YES;
}

- (void)disable
{
	_enabled = NO;
}

- (void)indent
{
	_indent += 1;
}

- (void)indent:(NSUInteger)tabs
{
	_indent += tabs;
}

- (void)unindent
{
	if ( _indent > 0 )
	{
		_indent -= 1;
	}
}

- (void)unindent:(NSUInteger)tabs
{
	if ( _indent < tabs )
	{
		_indent = 0;
	}
	else
	{
		_indent -= tabs;
	}
}

- (void)file:(NSString *)file line:(NSUInteger)line func:(NSString *)func level:(LogLevel)level format:(NSString *)format, ...
{
#if __SAMURAI_LOGGING__
	
	if ( nil == format || NO == [format isKindOfClass:[NSString class]] )
		return;
	
	va_list args;
	va_start( args, format );
	
	[self file:file line:line func:func level:level format:format args:args];
	
	va_end( args );
	
#endif
}

- (void)file:(NSString *)file line:(NSUInteger)line func:(NSString *)func level:(LogLevel)level format:(NSString *)format args:(va_list)params
{
#if __SAMURAI_LOGGING__
	
	if ( NO == _enabled || level > _filter )
		return;

	@autoreleasepool
	{
		const char * prefix = __prefix[level];
		if ( NULL == prefix )
		{
			prefix = "";
		}

		char tabs[256] = { 0 };
		for ( NSUInteger i = 0; i < _indent; ++i )
		{
			tabs[i] = '\t';
		}

		size_t plen = strlen(prefix);
		size_t diff = ((plen / 8 + 1) * 8) - plen;
		
		char padding[16] = { 0 };
		for ( size_t i = 0; i < diff; ++i )
		{
			padding[i] = ' ';
		}

		NSMutableString * content = [[NSMutableString alloc] initWithFormat:(NSString *)format arguments:params];
		if ( content )
		{
			if ( [content rangeOfString:@"\n"].length )
			{
				[content replaceOccurrencesOfString:@"\n"
										 withString:[NSString stringWithFormat:@"\n%s", _indent ? tabs : "\t\t"]
											options:NSCaseInsensitiveSearch
											  range:NSMakeRange(0, content.length)];
			}
		}

		NSMutableString * text = [NSMutableString new];
		if ( text )
		{
			[text appendFormat:@"%s%s%s", prefix, padding, tabs];
			[text appendString:content];

			if ( [text rangeOfString:@"%"].length )
			{
				[text replaceOccurrencesOfString:@"%"
									  withString:@"%%"
										 options:NSLiteralSearch
										   range:NSMakeRange(0, text.length)];
			}
			
			fprintf( stderr, "%s\n", [text UTF8String] );
		}
		
	#if __SAMURAI_DEBUG__
		SamuraiBacklog * log = [SamuraiBacklog new];
		if ( log )
		{
			log.level = level;
			log.text = text;
			log.file = file;
			log.line = line;
			log.func = func;

			[_lock lock];
			[_logs pushTail:log];
			[_logs keepTail:MAX_BACKLOG];
			[_lock unlock];
		}
	#endif
	}

#endif
}

@end

#pragma mark -

#if __cplusplus
extern "C"
#endif	// #if __cplusplus
void SamuraiLog( NSString * format, ... )
{
#if __ENABLE_NSLOG_HOOK__
	
	if ( nil == format || NO == [format isKindOfClass:[NSString class]] )
		return;
	
	va_list args;
	va_start( args, format );

	[[SamuraiLogger sharedInstance] file:nil line:0 func:nil level:LogLevel_Info format:format args:args];

	va_end( args );

#else
	
	UNUSED( format );
	
#endif
}
