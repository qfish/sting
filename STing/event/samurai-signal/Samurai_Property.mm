//
//     ____    _                        __     _      _____
//    / ___\  /_\     /\/\    /\ /\    /__\   /_\     \_   \
//    \ \    //_\\   /    \  / / \ \  / \//  //_\\     / /\/
//  /\_\ \  /  _  \ / /\/\ \ \ \_/ / / _  \ /  _  \ /\/ /_
//  \____/  \_/ \_/ \/    \/  \___/  \/ \_/ \_/ \_/ \____/
//
//	Copyright Samurai development team and other contributors
//
//	http://www.samurai-framework.com
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy
//	of this software and associated documentation files (the "Software"), to deal
//	in the Software without restriction, including without limitation the rights
//	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//	copies of the Software, and to permit persons to whom the Software is
//	furnished to do so, subject to the following conditions:
//
//	The above copyright notice and this permission notice shall be included in
//	all copies or substantial portions of the Software.
//
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//	THE SOFTWARE.
//

#import "Samurai_Property.h"

// ----------------------------------
// Source code
// ----------------------------------

#pragma mark -

@implementation NSObject(Property)

//+ (const char *)attributesForProperty:(NSString *)property
//{
//	Class baseClass = [self baseClass];
//	if ( nil == baseClass )
//	{
//		baseClass = [NSObject class];
//	}
//
//	for ( Class clazzType = self; clazzType != baseClass; )
//	{
//		objc_property_t prop = class_getProperty( clazzType, [property UTF8String] );
//		if ( prop )
//		{
//			return property_getAttributes( prop );
//		}
//		
//		clazzType = class_getSuperclass( clazzType );
//		if ( nil == clazzType )
//			break;
//	}
//
//	return NULL;
//}

- (const char *)attributesForProperty:(NSString *)property
{
	return [[self class] attributesForProperty:property];
}

//+ (NSDictionary *)extentionsForProperty:(NSString *)property
//{
//	SEL fieldSelector = NSSelectorFromString( [NSString stringWithFormat:@"property_%@", property] );
//	if ( [self respondsToSelector:fieldSelector] )
//	{
//		NSString * field = [self performSelector:fieldSelector];
//		if ( field )
//		{
//			NSMutableDictionary * dict = [NSMutableDictionary dictionary];
//			
//			NSArray * attributes = [field componentsSeparatedByString:@"____"];
//			for ( NSString * attrGroup in attributes )
//			{
//				NSArray *	groupComponents = [attrGroup componentsSeparatedByString:@"=>"];
//				NSString *	groupName = [[[groupComponents safeObjectAtIndex:0] trim] unwrap];
//				NSString *	groupValue = [[[groupComponents safeObjectAtIndex:1] trim] unwrap];
//
//				if ( groupName && groupValue )
//				{
//					[dict setObject:groupValue forKey:groupName];
//				}
//			}
//			
//			return dict;
//		}
//	}
//	
//	return nil;
//}

- (NSDictionary *)extentionsForProperty:(NSString *)property
{
	return [[self class] extentionsForProperty:property];
}

- (id)getAssociatedObjectForKey:(const char *)key
{
	const char * propName = key; // [[NSString stringWithFormat:@"%@.%s", NSStringFromClass([self class]), key] UTF8String];
	
	id currValue = objc_getAssociatedObject( self, propName );
	return currValue;
}

- (id)copyAssociatedObject:(id)obj forKey:(const char *)key
{
	const char * propName = key; // [[NSString stringWithFormat:@"%@.%s", NSStringFromClass([self class]), key] UTF8String];
	
	id oldValue = objc_getAssociatedObject( self, propName );
	objc_setAssociatedObject( self, propName, obj, OBJC_ASSOCIATION_COPY );
	return oldValue;
}

- (id)retainAssociatedObject:(id)obj forKey:(const char *)key;
{
	const char * propName = key; // [[NSString stringWithFormat:@"%@.%s", NSStringFromClass([self class]), key] UTF8String];
	
	id oldValue = objc_getAssociatedObject( self, propName );
	objc_setAssociatedObject( self, propName, obj, OBJC_ASSOCIATION_RETAIN_NONATOMIC );
	return oldValue;
}

- (id)assignAssociatedObject:(id)obj forKey:(const char *)key
{
	const char * propName = key; // [[NSString stringWithFormat:@"%@.%s", NSStringFromClass([self class]), key] UTF8String];
	
	id oldValue = objc_getAssociatedObject( self, propName );
	objc_setAssociatedObject( self, propName, obj, OBJC_ASSOCIATION_ASSIGN );
	return oldValue;
}

- (void)removeAssociatedObjectForKey:(const char *)key
{
	const char * propName = key; // [[NSString stringWithFormat:@"%@.%s", NSStringFromClass([self class]), key] UTF8String];

	objc_setAssociatedObject( self, propName, nil, OBJC_ASSOCIATION_ASSIGN );
}

- (void)removeAllAssociatedObjects
{
	objc_removeAssociatedObjects( self );
}

@end
