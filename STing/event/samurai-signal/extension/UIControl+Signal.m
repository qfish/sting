//
//  UIControl+Signal.m
//  mplus
//
//  Created by QFish on 10/17/14.
//  Copyright (c) 2014 geek-zoo. All rights reserved.
//

#import "Samurai_Signal.h"
#import "Samurai_SignalBus.h"
#import "UIControl+Signal.h"
#import <objc/objc.h>

static void * kSignalKey;

@implementation UIControl (Signal)

@dynamic signal;

- (void)setSignal:(NSString *)signal
{
    objc_setAssociatedObject(self, kSignalKey, signal, OBJC_ASSOCIATION_COPY_NONATOMIC);
    [self addTarget:self action:@selector(_signal_clicked:) forControlEvents:UIControlEventTouchUpInside];
}

- (NSString *)signal
{
    return objc_getAssociatedObject(self, kSignalKey);
}

- (void)_signal_clicked:(id)sender
{
    [self sendSignal:[self signal] from:sender];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    
}

@end
