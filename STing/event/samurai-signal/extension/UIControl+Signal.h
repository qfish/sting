//
//  UIControl+Signal.h
//  mplus
//
//  Created by QFish on 10/17/14.
//  Copyright (c) 2014 geek-zoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIControl (Signal)
@property (nonatomic, strong) NSString * signal;
@end
