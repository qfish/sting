// {Prefix}

#pragma mark -

@interface NSString(BeeExtension)

@property (nonatomic, readonly) NSData * data;

@property (nonatomic, readonly) NSString * URLDecoding;
@property (nonatomic, readonly) NSString * URLEncoding;

@property (nonatomic, readonly) NSString * MD5;
@property (nonatomic, readonly) NSString * SHA1;

@end
